package com.example.prototypese;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;

import java.util.Vector;

public class SurveyHistoryActivity extends AppCompatActivity {

    RecyclerView rv_survey_history;
    Vector<Survey> surveyHistoryVector;
    SurveyHistoryAdapter surveyHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_history);

        renderBottomNavbar();
        rv_survey_history = findViewById(R.id.rv_survey_history);

        surveyHistoryVector = new Vector<>();
//        surveyHistoryVector.add(new Survey("Upaya Peningkatan Keefektifan dan Efisiensi Pembelajaran Bahasa Inggris pada Siswa", "ayam.1218", " ", "12 Februari 2014",
//                30000, 50, 29, "iewfiowofwwe;"));
//        surveyHistoryVector.add(new Survey("Upaya Peningkatan Keefektifan dan Efisiensi Pembelajaran Bahasa Inggris pada Siswa", "ayam.1218", " ", "22 Oktober 2005",
//                30000, 50, 29, "iewfiowofwwe;"));
//        surveyHistoryVector.add(new Survey("Aplikasi Text To Speech (TTS) Berbahasa Indonesia Sebagai Pembaca SMS", "lilie10_", " ", "17 Oktober 2020",
//                25000, 50, 17, "awdbakwjdbkawndlaw"));
//        surveyHistoryVector.add(new Survey("Aplikasi Logika Matematika pada Penyusunan Jaringan Listrik", "park_GG", " ", "13 Maret 2006",
//                15000, 50, 21, "asdwadghijwwl;"));

        surveyHistoryAdapter = new SurveyHistoryAdapter(this);
        surveyHistoryAdapter.setSurveyHistoryVector(surveyHistoryVector);

        rv_survey_history.setAdapter(surveyHistoryAdapter);
        rv_survey_history.setLayoutManager(new LinearLayoutManager(this));
    }

    private void renderBottomNavbar()
    {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        bottomNavigationView.setSelectedItemId(R.id.menuHistory);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(MenuItem item)
            {
                Intent intent;
                switch (item.getItemId())
                {
                    case R.id.menuHome:
                        Toast.makeText(getApplicationContext(), "Home", Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        startActivity(intent);
                        finish();
                        break;

                    case R.id.menuSearch:
                        Toast.makeText(getApplicationContext(), "Search", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.menuSurvey:
                        Toast.makeText(getApplicationContext(), "Survey", Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(), YourSurveyActivity.class);
                        startActivity(intent);
                        finish();
                        break;

                    case R.id.menuHistory:
                        Toast.makeText(getApplicationContext(), "History", Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(), SurveyHistoryActivity.class);
                        startActivity(intent);
                        finish();
                        break;

                    case R.id.menuProfile:
                        Toast.makeText(getApplicationContext(), "Profile", Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });
    }

}