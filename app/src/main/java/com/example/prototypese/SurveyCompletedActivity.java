package com.example.prototypese;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SurveyCompletedActivity extends AppCompatActivity {

    int user_id, survey_id, participant;
    long reward;
    TextView tv_reward;
	Button btn_yeay;
	SurveyDB surveyDB;
	UserDB userDB;
	Users user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_completed);
        userDB = new UserDB(this);
        surveyDB = new SurveyDB(this);
        user_id = getIntent().getIntExtra("user_id", 0);
        user = userDB.getUserByID(user_id);
        reward = getIntent().getLongExtra("reward", 0);
        survey_id = getIntent().getIntExtra("survey_id", 0);
        participant = getIntent().getIntExtra("participant", 0);
        tv_reward = findViewById(R.id.tv_reward);
		btn_yeay = findViewById(R.id.btn_yeay);

		tv_reward.setText("Rp. " + reward);

		btn_yeay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                userDB.updateWallet(user_id, user.getBalance(), reward);
                surveyDB.updateSurvey(survey_id, participant);
                intent.putExtra("user_id", user_id);
                startActivity(intent);
            }
        });

    }
}
