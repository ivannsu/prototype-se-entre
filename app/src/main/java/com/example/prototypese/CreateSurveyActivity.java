package com.example.prototypese;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CreateSurveyActivity extends AppCompatActivity {

    EditText et_survey_title, et_survey_desc, et_survey_reward, et_survey_participant, et_survey_url;
    Button btn_share;
    Date date;
    SurveyDB surveyDB;
    Survey survey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_survey);

        surveyDB = new SurveyDB(this);
        date = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        String created_at = dateFormat.format(date);

        int user_id = getIntent().getIntExtra("user_id", 0);
        Log.d("user_id_create", String.valueOf(user_id));
        et_survey_title = findViewById(R.id.et_survey_title);
        et_survey_desc = findViewById(R.id.et_survey_desc);
        et_survey_reward = findViewById(R.id.et_survey_reward);
        et_survey_participant = findViewById(R.id.et_survey_participant);
        et_survey_url = findViewById(R.id.et_survey_url);
        btn_share = findViewById(R.id.btn_share);

        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = et_survey_title.getText().toString();
                String author = getIntent().getStringExtra("username");
                String desc = et_survey_desc.getText().toString();
                long reward = Long.parseLong(et_survey_reward.getText().toString());
                int max_participant = Integer.parseInt(et_survey_participant.getText().toString());
                String url = et_survey_url.getText().toString();

                survey = new Survey();
                survey.setUserId(user_id);
                survey.setSurvey_title(title);
                survey.setSurvey_author(author);
                survey.setSurvey_desc(desc);
                survey.setCreated_at(created_at);
                survey.setSurvey_reward(reward);
                survey.setSurvey_participant(0);
                survey.setSurvey_max_participant(max_participant);
                survey.setSurvey_url(url);
                surveyDB.insertSurvey(survey);
                Toast.makeText(getApplicationContext(),"Survey has been shared",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), YourSurveyActivity.class);
                intent.putExtra("user_id", user_id);
                startActivity(intent);
            }
        });

    }
}