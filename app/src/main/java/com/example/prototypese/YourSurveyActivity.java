package com.example.prototypese;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;

import java.util.Vector;

public class YourSurveyActivity extends AppCompatActivity {

    RecyclerView rv_your_survey;
    Vector<Survey> yourSurveyVector;
    YourSurveyAdapter yourSurveyAdapter;
    SurveyDB surveyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_survey);

        int userId = getIntent().getIntExtra("user_id", 0);
        renderBottomNavbar(userId);
        surveyDB = new SurveyDB(this);
        rv_your_survey = findViewById(R.id.rv_your_survey);
        Log.d("user_id_your", String.valueOf(userId));
        yourSurveyVector = new Vector<>();
        yourSurveyVector = surveyDB.getYourSurvey(userId);
        Log.d("size", String.valueOf(yourSurveyVector.size()));
        yourSurveyAdapter = new YourSurveyAdapter(this);
        yourSurveyAdapter.setYourSurveyVector(yourSurveyVector);

        rv_your_survey.setAdapter(yourSurveyAdapter);
        rv_your_survey.setLayoutManager(new LinearLayoutManager(this));

    }

    private void renderBottomNavbar(int user_id)
    {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        bottomNavigationView.setSelectedItemId(R.id.menuSurvey);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(MenuItem item)
            {
                Intent intent;
                switch (item.getItemId())
                {
                    case R.id.menuHome:
                        Toast.makeText(getApplicationContext(), "Home", Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.putExtra("user_id", user_id);
                        startActivity(intent);
                        finish();
                        break;

                    case R.id.menuSearch:
                        Toast.makeText(getApplicationContext(), "Search", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.menuSurvey:
                        Toast.makeText(getApplicationContext(), "Survey", Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(), YourSurveyActivity.class);
                        intent.putExtra("user_id", user_id);
                        startActivity(intent);
                        finish();
                        break;

                    case R.id.menuHistory:
                        Toast.makeText(getApplicationContext(), "History", Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(), SurveyHistoryActivity.class);
                        intent.putExtra("user_id", user_id);
                        startActivity(intent);
                        finish();
                        break;

                    case R.id.menuProfile:
                        Toast.makeText(getApplicationContext(), "Profile", Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });
    }
}