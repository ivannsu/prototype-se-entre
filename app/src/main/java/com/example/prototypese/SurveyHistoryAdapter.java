package com.example.prototypese;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Vector;

public class SurveyHistoryAdapter extends RecyclerView.Adapter<SurveyHistoryAdapter.ViewHolder> {

    Context ctx;
    Vector<Survey> surveyHistoryVector;

    public SurveyHistoryAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public void setSurveyHistoryVector(Vector<Survey> surveyHistoryVector) {
        this.surveyHistoryVector = surveyHistoryVector;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.item_survey_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tv_survey_title.setText(surveyHistoryVector.get(position).getSurvey_title());
        holder.tv_created_at.setText(surveyHistoryVector.get(position).getCreated_at());
        holder.tv_reward.setText("Rp. " + String.valueOf(surveyHistoryVector.get(position).getSurvey_reward()));
    }

    @Override
    public int getItemCount() {
        return surveyHistoryVector.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_survey_title, tv_created_at, tv_reward;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_survey_title = itemView.findViewById(R.id.tv_survey_title);
            tv_created_at = itemView.findViewById(R.id.tv_created_at);
            tv_reward = itemView.findViewById(R.id.tv_reward);
        }
    }
}
