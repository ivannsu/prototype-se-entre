package com.example.prototypese;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Vector;

public class HomeActivity extends AppCompatActivity {

    RecyclerView rv_survey;
    Vector<Survey> surveyVector;
    SurveyAdapter surveyAdapter;
    FloatingActionButton fab_create;
    SurveyDB surveyDB;
    UserDB userDB;
    Users user;
    TextView tv_welcome, tv_balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        userDB = new UserDB(this);
        surveyDB = new SurveyDB(this);
        int user_id = getIntent().getIntExtra("user_id", 0);
        user = userDB.getUserByID(user_id);
        renderBottomNavbar(user_id);
        tv_welcome = findViewById(R.id.tv_welcome);
        tv_balance = findViewById(R.id.tv_balance);
        rv_survey = findViewById(R.id.rv_survey);
        fab_create = findViewById(R.id.fab_create);

        tv_welcome.setText("Welcome, " + user.getUsername());
        tv_balance.setText("Your balance: Rp. " + user.getBalance());

        surveyVector = new Vector<>();
        surveyVector = surveyDB.getAllSurvey();

        surveyAdapter = new SurveyAdapter(this);
        surveyAdapter.setSurveyVector(surveyVector);

        rv_survey.setAdapter(surveyAdapter);
        rv_survey.setLayoutManager(new LinearLayoutManager(this));

        fab_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = getIntent().getStringExtra("username");
                Toast.makeText(getApplicationContext(), "Create a Survey", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), CreateSurveyActivity.class);
                intent.putExtra("user_id", user_id);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });

    }

    private void renderBottomNavbar(int user_id)
    {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        bottomNavigationView.setSelectedItemId(R.id.menuHome);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(MenuItem item)
            {
                Intent intent;
                switch (item.getItemId())
                {
                    case R.id.menuHome:
                        Toast.makeText(getApplicationContext(), "Home", Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.putExtra("user_id", user_id);
                        startActivity(intent);
                        finish();
                        break;

                    case R.id.menuSearch:
                        Toast.makeText(getApplicationContext(), "Search", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.menuSurvey:
                        Toast.makeText(getApplicationContext(), "Survey", Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(), YourSurveyActivity.class);
                        intent.putExtra("user_id", user_id);
                        startActivity(intent);
                        finish();
                        break;

                    case R.id.menuHistory:
                        Toast.makeText(getApplicationContext(), "History", Toast.LENGTH_SHORT).show();
                        intent = new Intent(getApplicationContext(), SurveyHistoryActivity.class);
                        intent.putExtra("user_id", user_id);
                        startActivity(intent);
                        finish();
                        break;

                    case R.id.menuProfile:
                        Toast.makeText(getApplicationContext(), "Profile", Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });
    }

}